//
//  GameScene.swift
//  Pong
//
//  Created by Michał Podgórni on 09/02/2019.
//  Copyright © 2019 Michał Podgórni. All rights reserved.
//

import UIKit
import SpriteKit
import GameplayKit

class GameScene: SKScene {
    
    var ball = SKSpriteNode()
    var enemy = SKSpriteNode()
    var player = SKSpriteNode()
    
    var playerLabel = SKLabelNode()
    var enemyLabel = SKLabelNode()
    
    var score = [Int]()
    
    override func didMove(to view: SKView) {
        
        startGame()
        
        ball = self.childNode(withName: "ball") as! SKSpriteNode
        enemy = self.childNode(withName: "enemy") as! SKSpriteNode
        player = self.childNode(withName: "main") as! SKSpriteNode
        
        ball.physicsBody?.applyImpulse(CGVector(dx: 40, dy: 40))
        let border = SKPhysicsBody(edgeLoopFrom: self.frame)
        border.friction = 0
        border.restitution = 1
        
        self.physicsBody = border
    }
    
    func startGame() {
        score = [0,0]
    }
    
    func addScore(playerWhoWon : SKSpriteNode) {
        
        let random = Int.random(in: -80...80)
        
        ball.position = CGPoint(x: 0, y: random)
        ball.physicsBody?.velocity = CGVector(dx: 0, dy: 0);
        
        let powerLevel = (Int(Double(score[0]) * 4.77))
        let colorChanger: Double = 0.047
        
        if(score[0]==20 || score[1]==20) {
            startGame()
            playerLabel.text = String(score[0])
            playerLabel.fontColor = UIColor(white: 0, alpha: 0.1)
            enemyLabel.text = String(score[1])
            enemyLabel.fontColor = UIColor(white: 0, alpha: 0.1)
            ball.physicsBody?.applyImpulse(CGVector(dx: 40, dy: 40))
        } else {
            if(playerWhoWon == player) {
                score[0] += 1
                playerLabel = self.childNode(withName : "mainLabel") as! SKLabelNode
                playerLabel.text = String(score[0])
                playerLabel.fontColor = UIColor(white: 0, alpha: CGFloat(0.1 + (colorChanger * Double(score[0]))))
                ball.physicsBody?.applyImpulse(CGVector(dx: 40 + powerLevel, dy: 40 + powerLevel))
            } else if(playerWhoWon == enemy) {
                score[1] += 1
                enemyLabel = self.childNode(withName : "enemyLabel") as! SKLabelNode
                enemyLabel.text = String(score[1])
                enemyLabel.fontColor = UIColor(white: 0, alpha: CGFloat(0.1 + (colorChanger * Double(score[1]))))
                ball.physicsBody?.applyImpulse(CGVector(dx: -40 - powerLevel, dy: -40 - powerLevel))
            }
        }
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        for touch in touches {
            let location = touch.location(in: self)
            player.run(SKAction.moveTo(x: location.x, duration: 0))
        } 
    }
    
    override func update(_ currentTime: TimeInterval) {
        enemy.run(SKAction.moveTo(x: ball.position.x, duration: 0.62 - (0.27 + 0.019 *  Double(score[0]))))
        
        if ball.position.y <= player.position.y - 100 {
            addScore(playerWhoWon: enemy)
        } else if (ball.position.y >= enemy.position.y + 100) {
            addScore(playerWhoWon: player)
        }
        
    }
}
